from django.urls import path
from .views import *


urlpatterns = [
    #serve index.html
    path("", index, name="index"),
    #GET, POST books
    path("books", BookList.as_view(), name="book_list"),
    path("books/<int:id>", BookDetail.as_view(), name="book_detail"), 
]
from re import I
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework.views import APIView
from rest_framework import status

from .models import Book
from .serializers import BookSerializer #permet de vérifier le format du Book, qu'il n y ait pas n'imp dedans

# Create your views here.
def index(request):
    return HttpResponse("hello there. You are at the myapp index")

class BookList(APIView):
    #List all books, or create new book
    def get(self,request, format=None):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self, request, format=None): 
        serializer = BookSerializer(data=request.data)  #donnée de book dans la request
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe=False, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, safe=False, status=status.HTTP_400_BAD_REQUEST)

class BookDetail(APIView):
    def get_object(self,id):
        try:
            return Book.objects.get(id=id)
        except Book.DoesNotExist:
            raise Http404
    def get(self,request, id, format=None): #dans la requete, id du book

        book = self.get_object(id)
        serializer = BookSerializer(book, many=False)
        return JsonResponse(serializer.data, safe=False)
    def put(self,request,id, format=None):
        book = self.get_object(id)
        serializer = BookSerializer(book, data=request.data)  #donnée de book dans la request
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, safe=False, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,id, format=None):
        book = self.get_object(id)
        book.delete()
        return JsonResponse({}, safe=False,status=status.HTTP_204_NO_CONTENT)
